﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void PlayAgain()
    {
        SceneManager.LoadScene("Gameplay");
        ScoreScript.playerScore = 0;
    }
    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        ScoreScript.playerScore = 0;
    }
}