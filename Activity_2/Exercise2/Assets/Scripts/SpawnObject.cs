﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour {
    public GameObject cannon;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space))
        {
            PowerText.ammo -= 1;
            FindObjectOfType<AudioManager>().Play("Cannon");
            Instantiate(cannon, this.transform.position, transform.rotation);
        }
	}
}
