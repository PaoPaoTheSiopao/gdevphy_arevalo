﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PowerText : MonoBehaviour {

    Text scoreText;
    public static int power;
    public static int ammo;

    void Start()
    {
        scoreText = GetComponent<Text>();
        //GameObject obj = GameObject.FindGameObjectWithTag("Arrow");
        //Arrow objs = obj.GetComponent<Arrow>();

    }
    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Power: " + power + "\n" + "Ammo: "+ ammo;
        if(ammo<=0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
