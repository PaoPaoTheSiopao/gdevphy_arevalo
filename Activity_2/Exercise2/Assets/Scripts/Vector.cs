﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vector{
    public float x;
    public float y;
    public float z;

    public Vector(float x, float y, float z)
    {
        this.x = x * Time.deltaTime;
        this.y = y * Time.deltaTime;
        this.z = z * Time.deltaTime;
    }

    public float ConvertPosToAngle()
    {
        float angle = Mathf.Atan2(this.y, this.x);
        return angle;
    }
}
