﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Velocity : MonoBehaviour {
    //public float angle = 45;
    //Vector velo;
    Vector velocity;
    Vector acceleration;
    //Vector gravity;
    public float xUnit;
    public float yUnit;
    public float mass;
    public Vector mForce;
    float x;
    float y;
    public float speed = 5;
    public float lastAngle;
    public float throwForce;
    public float posAngle;
    public float gravity;
    float xDegToRad;
    float yDegToRad;
    float xCoor;
    float yCoor;
    //GameObject ang = GameObject.FindGameObjectWithTag("Arrow");
    

    // Use this for initialization
    void Start () {

        GameObject ang = GameObject.FindGameObjectWithTag("Arrow");
        Arrow arrowAngle = ang.GetComponent<Arrow>();
        posAngle = arrowAngle.arAngle;
        throwForce = arrowAngle.power;
        //velo = new Vector(1, 1, 0);
        mForce = new Vector(0, 10, 0);
        acceleration = new Vector(0, 0, 0);
        velocity = new Vector(0, 10, 0);
        //gravity = new Vector(0, -19.8f, 0);
        xDegToRad = Mathf.Cos(Mathf.Deg2Rad * posAngle);
        yDegToRad = Mathf.Sin(Mathf.Deg2Rad * posAngle);
        xCoor = throwForce * xDegToRad;
        yCoor = throwForce * yDegToRad;
        Debug.Log("xcoor" + xCoor);

    }
	
	// Update is called once per frame
	void Update () {


        AddForce(xCoor, yCoor);
        ApplyGravity(3);
        UpdateMotion();
        //Debug.Log("x= "+gravity.x + "y= "+ gravity.y);
        //Debug.Log("Mass =" + mass);
        //Debug.Log("velox= " + velocity.x + "y= " + velocity.y);
        //Debug.Log("xCoor =" + xCoor + "yCoor =" + yCoor);


    }

    void ComputeVelocity()
    {
        //velo.x = Mathf.Cos(Mathf.Deg2Rad*angle);
        //velo.y = Mathf.Sin(Mathf.Deg2Rad*angle);
    }
    
    void AddForce(float x, float y)
    {
        this.acceleration.x += (x / mass);
        this.acceleration.y += (y / mass);
        //Debug.Log("Accel =" + acceleration.x + " y =" + acceleration.y);
        //this.acceleration.y += (-2.9f / mass);
    }
    void UpdateMotion()
    {   
        this.velocity.x += this.acceleration.x * Time.deltaTime;
        this.velocity.y += this.acceleration.y * Time.deltaTime;
        transform.position = new Vector2(this.velocity.x, this.velocity.y);
        this.acceleration.x *= 0;
        this.acceleration.y *= 0;
    }

    public void ApplyGravity(float amount)
    {
        gravity += -amount * mass;
        this.acceleration.y += gravity;
        //Debug.Log("Gravity Y =" + acceleration.y);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject obj = collision.gameObject;
        Box objec = obj.GetComponent<Box>();
        objec.Addforce(new Vector2(1, 0));
        objec.ApplyGravity(1);
        Score.playerScore += 100;
        PowerText.ammo += 3;
        if (collision.gameObject.tag=="Box")
        {
            Debug.Log("box hit");
            
        }
    }
}
