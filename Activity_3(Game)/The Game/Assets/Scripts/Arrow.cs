﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {
    public float arAngle = 0;
    public int power = 0;
    float smooth = 5;
    int ammo = 5;
    public Velocity angle;
	// Use this for initialization
	void Start () {
        PowerText.ammo = ammo;
        power = 40;
        Score.playerScore = 0;
    }

    // Update is called once per frame
    void Update () {
        
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            arAngle += 5;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            arAngle += -5;
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            power += -5;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            power += 5;
        }
        if(power<0)
        {
            power = 0;
        }
        Quaternion target = Quaternion.Euler(0, 0, arAngle);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
        //transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
        //transform.Rotate(0, 0, 5 * Time.deltaTime);
        Debug.Log("angle = "+angle);
        PowerText.power = power;
        //PowerText.ammo = ammo;
    }
}
