﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour {

    public Vector2 accelaration = new Vector2(0, 0);
    public Vector2 velocity = new Vector2(0, 0);
    //Vector2 wind = new Vector2(0.1f, 0);
    public float mass = 1;
    //Vector2 player = new Vector2(0, 0);
    public Vector2 force = new Vector2(0, 0);
    public float gravity = 0;
    public float throwAngle;
    public float throwForce;
    float xDegToRad;
    float yDegToRad;
    Vector2 coordinates = new Vector2(0, 0);


    // Use this for initialization
    void Start()
    {
        xDegToRad = Mathf.Cos(Mathf.Deg2Rad * throwAngle);
        yDegToRad = Mathf.Sin(Mathf.Deg2Rad * throwAngle);
        coordinates.x = throwForce * xDegToRad;
        coordinates.y = throwForce * yDegToRad;
    }

    // Update is called once per frame
    void Update()
    {
        Addforce(coordinates);
        ApplyGravity(gravity);
        UpdateMotion();
        Debug.Log(velocity);
        if(this.transform.position.y<=-20)
        {
            Destroy(gameObject);
            SpawnBox.isOkayToSpawn = true;
        }
    }

    public void Addforce(Vector2 force)
    {
        this.accelaration += (force / mass);
    }

    private void UpdateMotion()
    {
        this.velocity += this.accelaration;
        this.transform.position = new Vector2(this.transform.position.x + this.velocity.x, this.transform.position.y + this.velocity.y);
        this.accelaration *= 0;
    }
    public void ApplyGravity(float amount)
    {
        gravity += -amount * mass;
        this.accelaration.y += gravity;
        //Debug.Log("Gravity Y =" + acceleration.y);
    }
}
