﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBox : MonoBehaviour {

    public static bool isOkayToSpawn = true;
    public GameObject box;

	// Use this for initialization
	void Start () {
        isOkayToSpawn = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (isOkayToSpawn)
        {
            Instantiate(box, new Vector3(this.transform.position.x + Random.Range(-5, 5), this.transform.position.y + Random.Range(-5, 5), 0), transform.rotation);
            isOkayToSpawn = false;
        }
        Debug.Log(isOkayToSpawn);
	}
}
