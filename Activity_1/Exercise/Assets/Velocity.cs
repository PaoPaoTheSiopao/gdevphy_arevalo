﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Velocity : MonoBehaviour {
    //public float angle = 45;
    Vector velo;
    Vector velocity;
    Vector acceleration;
    public float xUnit;
    public float yUnit;
    public float mass;
    public Vector mForce;
    float x;
    float y;
    public float speed = 5;
    public float lastAngle;
    // Use this for initialization
	void Start () {
        velo = new Vector(1, 1, 0);
        mForce = new Vector(0, 0, 0);
        acceleration = new Vector(0, 0, 0);
        velocity = new Vector(0, 0, 0);
    }
	
	// Update is called once per frame
	void Update () {
        //ComputeVelocity();
        xUnit = velo.x;
        yUnit = velo.y;
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");
        var angle = Mathf.Atan2(mForce.y, mForce.x) * Mathf.Rad2Deg;
        if (Input.anyKey)
        {
            lastAngle = angle;
        }
        //transform.position += new Vector3(velo.x, velo.y, 0)*Time.deltaTime;
        transform.position += new Vector3(x, y, 0) * (speed * Time.deltaTime) ;
        Debug.Log(lastAngle);
        if (Input.anyKey)
        {
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
        if(Input.GetKey(KeyCode.RightArrow))
        {
            mForce.x += (5 * Time.deltaTime);
        }
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            mForce.x -= (5 * Time.deltaTime);
        }
        if(Input.GetKey(KeyCode.UpArrow))
        {
            mForce.y += (5 * Time.deltaTime);
        }
        if(Input.GetKey(KeyCode.DownArrow))
        {
            mForce.y -= (5 * Time.deltaTime);
        }
        UpdateMotion();
        AddForce(mForce);
        Debug.Log("x= "+mForce.x + "y= "+ mForce.y);
        
	}

    void ComputeVelocity()
    {
        //velo.x = Mathf.Cos(Mathf.Deg2Rad*angle);
        //velo.y = Mathf.Sin(Mathf.Deg2Rad*angle);
    }
    
    void AddForce(Vector force)
    {
        this.acceleration.x = (force.x / mass);
        this.acceleration.y = (force.y / mass);
    }
    void UpdateMotion()
    {
        this.velocity.x += this.acceleration.x*Time.deltaTime;
        this.velocity.y += this.acceleration.y * Time.deltaTime;
        transform.position = new Vector2(this.velocity.x, this.velocity.y);
        this.acceleration.x *= 0;
        this.acceleration.y *= 0;
    }
    
}
